Model: "UNET"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input_1 (InputLayer)           [(None, 512, 512, 4  0           []                               
                                )]                                                                
                                                                                                  
 conv2d (Conv2D)                (None, 512, 512, 64  2368        ['input_1[0][0]']                
                                )                                                                 
                                                                                                  
 batch_normalization (BatchNorm  (None, 512, 512, 64  256        ['conv2d[0][0]']                 
 alization)                     )                                                                 
                                                                                                  
 activation (Activation)        (None, 512, 512, 64  0           ['batch_normalization[0][0]']    
                                )                                                                 
                                                                                                  
 conv2d_1 (Conv2D)              (None, 512, 512, 64  36928       ['activation[0][0]']             
                                )                                                                 
                                                                                                  
 batch_normalization_1 (BatchNo  (None, 512, 512, 64  256        ['conv2d_1[0][0]']               
 rmalization)                   )                                                                 
                                                                                                  
 activation_1 (Activation)      (None, 512, 512, 64  0           ['batch_normalization_1[0][0]']  
                                )                                                                 
                                                                                                  
 max_pooling2d (MaxPooling2D)   (None, 256, 256, 64  0           ['activation_1[0][0]']           
                                )                                                                 
                                                                                                  
 conv2d_2 (Conv2D)              (None, 256, 256, 12  73856       ['max_pooling2d[0][0]']          
                                8)                                                                
                                                                                                  
 batch_normalization_2 (BatchNo  (None, 256, 256, 12  512        ['conv2d_2[0][0]']               
 rmalization)                   8)                                                                
                                                                                                  
 activation_2 (Activation)      (None, 256, 256, 12  0           ['batch_normalization_2[0][0]']  
                                8)                                                                
                                                                                                  
 conv2d_3 (Conv2D)              (None, 256, 256, 12  147584      ['activation_2[0][0]']           
                                8)                                                                
                                                                                                  
 batch_normalization_3 (BatchNo  (None, 256, 256, 12  512        ['conv2d_3[0][0]']               
 rmalization)                   8)                                                                
                                                                                                  
 activation_3 (Activation)      (None, 256, 256, 12  0           ['batch_normalization_3[0][0]']  
                                8)                                                                
                                                                                                  
 max_pooling2d_1 (MaxPooling2D)  (None, 128, 128, 12  0          ['activation_3[0][0]']           
                                8)                                                                
                                                                                                  
 conv2d_4 (Conv2D)              (None, 128, 128, 25  295168      ['max_pooling2d_1[0][0]']        
                                6)                                                                
                                                                                                  
 batch_normalization_4 (BatchNo  (None, 128, 128, 25  1024       ['conv2d_4[0][0]']               
 rmalization)                   6)                                                                
                                                                                                  
 activation_4 (Activation)      (None, 128, 128, 25  0           ['batch_normalization_4[0][0]']  
                                6)                                                                
                                                                                                  
 conv2d_5 (Conv2D)              (None, 128, 128, 25  590080      ['activation_4[0][0]']           
                                6)                                                                
                                                                                                  
 batch_normalization_5 (BatchNo  (None, 128, 128, 25  1024       ['conv2d_5[0][0]']               
 rmalization)                   6)                                                                
                                                                                                  
 activation_5 (Activation)      (None, 128, 128, 25  0           ['batch_normalization_5[0][0]']  
                                6)                                                                
                                                                                                  
 max_pooling2d_2 (MaxPooling2D)  (None, 64, 64, 256)  0          ['activation_5[0][0]']           
                                                                                                  
 conv2d_6 (Conv2D)              (None, 64, 64, 512)  1180160     ['max_pooling2d_2[0][0]']        
                                                                                                  
 batch_normalization_6 (BatchNo  (None, 64, 64, 512)  2048       ['conv2d_6[0][0]']               
 rmalization)                                                                                     
                                                                                                  
 activation_6 (Activation)      (None, 64, 64, 512)  0           ['batch_normalization_6[0][0]']  
                                                                                                  
 conv2d_7 (Conv2D)              (None, 64, 64, 512)  2359808     ['activation_6[0][0]']           
                                                                                                  
 batch_normalization_7 (BatchNo  (None, 64, 64, 512)  2048       ['conv2d_7[0][0]']               
 rmalization)                                                                                     
                                                                                                  
 activation_7 (Activation)      (None, 64, 64, 512)  0           ['batch_normalization_7[0][0]']  
                                                                                                  
 max_pooling2d_3 (MaxPooling2D)  (None, 32, 32, 512)  0          ['activation_7[0][0]']           
                                                                                                  
 conv2d_8 (Conv2D)              (None, 32, 32, 1024  4719616     ['max_pooling2d_3[0][0]']        
                                )                                                                 
                                                                                                  
 batch_normalization_8 (BatchNo  (None, 32, 32, 1024  4096       ['conv2d_8[0][0]']               
 rmalization)                   )                                                                 
                                                                                                  
 activation_8 (Activation)      (None, 32, 32, 1024  0           ['batch_normalization_8[0][0]']  
                                )                                                                 
                                                                                                  
 conv2d_9 (Conv2D)              (None, 32, 32, 1024  9438208     ['activation_8[0][0]']           
                                )                                                                 
                                                                                                  
 batch_normalization_9 (BatchNo  (None, 32, 32, 1024  4096       ['conv2d_9[0][0]']               
 rmalization)                   )                                                                 
                                                                                                  
 activation_9 (Activation)      (None, 32, 32, 1024  0           ['batch_normalization_9[0][0]']  
                                )                                                                 
                                                                                                  
 conv2d_transpose (Conv2DTransp  (None, 64, 64, 512)  2097664    ['activation_9[0][0]']           
 ose)                                                                                             
                                                                                                  
 concatenate (Concatenate)      (None, 64, 64, 1024  0           ['conv2d_transpose[0][0]',       
                                )                                 'activation_7[0][0]']           
                                                                                                  
 conv2d_10 (Conv2D)             (None, 64, 64, 512)  4719104     ['concatenate[0][0]']            
                                                                                                  
 batch_normalization_10 (BatchN  (None, 64, 64, 512)  2048       ['conv2d_10[0][0]']              
 ormalization)                                                                                    
                                                                                                  
 activation_10 (Activation)     (None, 64, 64, 512)  0           ['batch_normalization_10[0][0]'] 
                                                                                                  
 conv2d_11 (Conv2D)             (None, 64, 64, 512)  2359808     ['activation_10[0][0]']          
                                                                                                  
 batch_normalization_11 (BatchN  (None, 64, 64, 512)  2048       ['conv2d_11[0][0]']              
 ormalization)                                                                                    
                                                                                                  
 activation_11 (Activation)     (None, 64, 64, 512)  0           ['batch_normalization_11[0][0]'] 
                                                                                                  
 conv2d_transpose_1 (Conv2DTran  (None, 128, 128, 25  524544     ['activation_11[0][0]']          
 spose)                         6)                                                                
                                                                                                  
 concatenate_1 (Concatenate)    (None, 128, 128, 51  0           ['conv2d_transpose_1[0][0]',     
                                2)                                'activation_5[0][0]']           
                                                                                                  
 conv2d_12 (Conv2D)             (None, 128, 128, 25  1179904     ['concatenate_1[0][0]']          
                                6)                                                                
                                                                                                  
 batch_normalization_12 (BatchN  (None, 128, 128, 25  1024       ['conv2d_12[0][0]']              
 ormalization)                  6)                                                                
                                                                                                  
 activation_12 (Activation)     (None, 128, 128, 25  0           ['batch_normalization_12[0][0]'] 
                                6)                                                                
                                                                                                  
 conv2d_13 (Conv2D)             (None, 128, 128, 25  590080      ['activation_12[0][0]']          
                                6)                                                                
                                                                                                  
 batch_normalization_13 (BatchN  (None, 128, 128, 25  1024       ['conv2d_13[0][0]']              
 ormalization)                  6)                                                                
                                                                                                  
 activation_13 (Activation)     (None, 128, 128, 25  0           ['batch_normalization_13[0][0]'] 
                                6)                                                                
                                                                                                  
 conv2d_transpose_2 (Conv2DTran  (None, 256, 256, 12  131200     ['activation_13[0][0]']          
 spose)                         8)                                                                
                                                                                                  
 concatenate_2 (Concatenate)    (None, 256, 256, 25  0           ['conv2d_transpose_2[0][0]',     
                                6)                                'activation_3[0][0]']           
                                                                                                  
 conv2d_14 (Conv2D)             (None, 256, 256, 12  295040      ['concatenate_2[0][0]']          
                                8)                                                                
                                                                                                  
 batch_normalization_14 (BatchN  (None, 256, 256, 12  512        ['conv2d_14[0][0]']              
 ormalization)                  8)                                                                
                                                                                                  
 activation_14 (Activation)     (None, 256, 256, 12  0           ['batch_normalization_14[0][0]'] 
                                8)                                                                
                                                                                                  
 conv2d_15 (Conv2D)             (None, 256, 256, 12  147584      ['activation_14[0][0]']          
                                8)                                                                
                                                                                                  
 batch_normalization_15 (BatchN  (None, 256, 256, 12  512        ['conv2d_15[0][0]']              
 ormalization)                  8)                                                                
                                                                                                  
 activation_15 (Activation)     (None, 256, 256, 12  0           ['batch_normalization_15[0][0]'] 
                                8)                                                                
                                                                                                  
 conv2d_transpose_3 (Conv2DTran  (None, 512, 512, 64  32832      ['activation_15[0][0]']          
 spose)                         )                                                                 
                                                                                                  
 concatenate_3 (Concatenate)    (None, 512, 512, 12  0           ['conv2d_transpose_3[0][0]',     
                                8)                                'activation_1[0][0]']           
                                                                                                  
 conv2d_16 (Conv2D)             (None, 512, 512, 64  73792       ['concatenate_3[0][0]']          
                                )                                                                 
                                                                                                  
 batch_normalization_16 (BatchN  (None, 512, 512, 64  256        ['conv2d_16[0][0]']              
 ormalization)                  )                                                                 
                                                                                                  
 activation_16 (Activation)     (None, 512, 512, 64  0           ['batch_normalization_16[0][0]'] 
                                )                                                                 
                                                                                                  
 conv2d_17 (Conv2D)             (None, 512, 512, 64  36928       ['activation_16[0][0]']          
                                )                                                                 
                                                                                                  
 batch_normalization_17 (BatchN  (None, 512, 512, 64  256        ['conv2d_17[0][0]']              
 ormalization)                  )                                                                 
                                                                                                  
 activation_17 (Activation)     (None, 512, 512, 64  0           ['batch_normalization_17[0][0]'] 
                                )                                                                 
                                                                                                  
 conv2d_18 (Conv2D)             (None, 512, 512, 1)  65          ['activation_17[0][0]']          
                                                                                                  
==================================================================================================
Total params: 31,055,873
Trainable params: 31,044,097
Non-trainable params: 11,776
__________________________________________________________________________________________________
