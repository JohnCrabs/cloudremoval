import os
import numpy as np
import satellitedatacube.src.Datacubes as cube
import lib.UnetCR as unet_cr

SENTINEL_1: str = cube.SENTINEL_1
SENTINEL_2: str = cube.SENTINEL_2
SENTINEL_3: str = cube.SENTINEL_3
LANDSAT_8: str = cube.LANDSAT_8
LANDSAT_9: str = cube.LANDSAT_9

UNET_PATH = "models/unet4sentinel2/unet.h5"


class CloudRemoval:
    def __init__(self, sourcePath: str, satelliteMission: str,
                 f_startDate=None, f_endDate=None,
                 f_minLat=None, f_minLon=None,
                 f_maxLat=None, f_maxLon=None,
                 f_resolutionMultiple=None, f_recreate_if_exist=False,
                 exportName: str = "datacube.nc"
                 ):
        cube_path = os.path.normpath(
            sourcePath + f"/{exportName}")
        self.datacube = cube.Datacubes()
        if not os.path.exists(cube_path) or f_recreate_if_exist:
            if os.path.exists(sourcePath) and os.path.isdir(sourcePath):
                self.datacube.create_datacube_from_folder(
                    folderPath=sourcePath,
                    f_startDate=f_startDate, f_endDate=f_endDate,
                    f_minLat=f_minLat, f_minLon=f_minLon,
                    f_maxLat=f_maxLat, f_maxLon=f_maxLon,
                    f_resolution_multiple=f_resolutionMultiple)
        else:
            self.datacube.importNetCDF(cube_path, satelliteMission)

        success, ds = self.datacube.getDatacube(
            satelliteMission=satelliteMission, as_dict=True)
        if success:
            unet = unet_cr.UnetCR(input_shape=(512, 512, 4),
                                  unet_path=UNET_PATH)

            if satelliteMission is SENTINEL_2:
                data_red = np.array(
                    ds[cube.KEY_DATA_VARS]['B2'][cube.KEY_DATA])
                data_green = np.array(
                    ds[cube.KEY_DATA_VARS]['B3'][cube.KEY_DATA])
                data_blue = np.array(
                    ds[cube.KEY_DATA_VARS]['B4'][cube.KEY_DATA])
                data_nir = np.array(
                    ds[cube.KEY_DATA_VARS]['B8'][cube.KEY_DATA])

                inData = np.array(
                    [data_red.T, data_green.T, data_blue.T, data_nir.T]).T

                dataSize = inData.shape[0]
                values = []
                for __index__ in range(0, dataSize):
                    curr_img = inData[__index__]
                    curr_img = np.expand_dims(curr_img, axis=0)
                    out_data = np.squeeze(unet.predict(curr_img)).tolist()
                    values.append(out_data)
                self.datacube.addDataLayer(values=values,
                                           satelliteMission=satelliteMission,
                                           label_name="cloud_mask")

        self.datacube.exportNetCDF(path=cube_path,
                                   satelliteMission=satelliteMission)


if __name__ == "__main__":
    PATH_DATA_FOLDER_SENTINEL_Piraeus = 'C:/Users/kavou/Desktop/Piraeus'
    PATH_DATA_FOLDER_SENTINEL_Sofia = 'C:/Users/kavou/Desktop/Sofia'
    PATH_DATA_FOLDER_SENTINEL_Ixelles = 'C:/Users/kavou/Desktop/Ixelles'
    PATH_DATA_FOLDER_SENTINEL_Milan = 'C:/Users/kavou/Desktop/Milan'

    NAME_NC_SENTINEL_Piraeus = 'Piraeus.nc'
    NAME_NC_SENTINEL_Sofia = 'Sofia.nc'
    NAME_NC_SENTINEL_Ixelles = 'Ixelles.nc'
    NAME_NC_SENTINEL_Milan = 'Milan.nc'

    # FILTER_START_DATE = dt.date(2016, 3, 1)
    # FILTER_END_DATE = dt.date(2017, 3, 31)

    FILTER_START_DATE = None
    FILTER_END_DATE = None
    FILTER_RESOLUTION_MULTIPLIED = 512

    # FILTER_MIN_LAT_S2_PIRAEUS = 37.925
    # FILTER_MIN_LON_S2_PIRAEUS = 23.615
    # FILTER_MAX_LAT_S2_PIRAEUS = 37.955
    # FILTER_MAX_LON_S2_PIRAEUS = 23.665

    FILTER_MIN_LAT_S2_PIRAEUS = 37.925
    FILTER_MIN_LON_S2_PIRAEUS = 23.615
    FILTER_MAX_LAT_S2_PIRAEUS = 37.965
    FILTER_MAX_LON_S2_PIRAEUS = 23.655

    FILTER_MIN_LAT_S2_SOFIA = 42.675
    FILTER_MIN_LON_S2_SOFIA = 23.300
    FILTER_MAX_LAT_S2_SOFIA = 42.720
    FILTER_MAX_LON_S2_SOFIA = 23.355

    FILTER_MIN_LAT_S2_IXELLES = 50.80
    FILTER_MIN_LON_S2_IXELLES = 4.350
    FILTER_MAX_LAT_S2_IXELLES = 50.845
    FILTER_MAX_LON_S2_IXELLES = 4.420

    FILTER_MIN_LAT_S2_MILAN = 45.440
    FILTER_MIN_LON_S2_MILAN = 9.135
    FILTER_MAX_LAT_S2_MILAN = 45.500
    FILTER_MAX_LON_S2_MILAN = 9.230

    CloudRemoval(
        PATH_DATA_FOLDER_SENTINEL_Piraeus,
        satelliteMission=SENTINEL_2,
        f_startDate=FILTER_START_DATE,
        f_endDate=FILTER_END_DATE,
        f_minLat=FILTER_MIN_LAT_S2_PIRAEUS,
        f_minLon=FILTER_MIN_LON_S2_PIRAEUS,
        f_maxLat=FILTER_MAX_LAT_S2_PIRAEUS,
        f_maxLon=FILTER_MAX_LON_S2_PIRAEUS,
        f_resolutionMultiple=FILTER_RESOLUTION_MULTIPLIED,
        f_recreate_if_exist=False,
        exportName=NAME_NC_SENTINEL_Piraeus
    )

    CloudRemoval(
        PATH_DATA_FOLDER_SENTINEL_Sofia,
        satelliteMission=SENTINEL_2,
        f_startDate=FILTER_START_DATE,
        f_endDate=FILTER_END_DATE,
        f_minLat=FILTER_MIN_LAT_S2_SOFIA,
        f_minLon=FILTER_MIN_LON_S2_SOFIA,
        f_maxLat=FILTER_MAX_LAT_S2_SOFIA,
        f_maxLon=FILTER_MAX_LON_S2_SOFIA,
        f_resolutionMultiple=FILTER_RESOLUTION_MULTIPLIED,
        f_recreate_if_exist=False,
        exportName=NAME_NC_SENTINEL_Sofia
    )

    CloudRemoval(
        PATH_DATA_FOLDER_SENTINEL_Ixelles,
        satelliteMission=SENTINEL_2,
        f_startDate=FILTER_START_DATE,
        f_endDate=FILTER_END_DATE,
        f_minLat=FILTER_MIN_LAT_S2_IXELLES,
        f_minLon=FILTER_MIN_LON_S2_IXELLES,
        f_maxLat=FILTER_MAX_LAT_S2_IXELLES,
        f_maxLon=FILTER_MAX_LON_S2_IXELLES,
        f_resolutionMultiple=FILTER_RESOLUTION_MULTIPLIED,
        f_recreate_if_exist=False,
        exportName=NAME_NC_SENTINEL_Ixelles
    )

    CloudRemoval(
        PATH_DATA_FOLDER_SENTINEL_Milan,
        satelliteMission=SENTINEL_2,
        f_startDate=FILTER_START_DATE,
        f_endDate=FILTER_END_DATE,
        f_minLat=FILTER_MIN_LAT_S2_MILAN,
        f_minLon=FILTER_MIN_LON_S2_MILAN,
        f_maxLat=FILTER_MAX_LAT_S2_MILAN,
        f_maxLon=FILTER_MAX_LON_S2_MILAN,
        f_resolutionMultiple=FILTER_RESOLUTION_MULTIPLIED,
        f_recreate_if_exist=False,
        exportName=NAME_NC_SENTINEL_Milan
    )
