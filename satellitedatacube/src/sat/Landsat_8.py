import os
import tarfile

import datetime as dt
import numpy as np
import rasterio as rs
from rasterio.plot import show
from PIL import Image

L8_MAIN_BANDS = ['B1', 'B2', 'B3', 'B4', 'B5',
                 'B6', 'B7', 'B8', 'B9', 'B10', 'B12']

L8_BAND_SPECTRAL = {
    'B1': "Coastal-Aerosol",
    'B2': "Blue",
    'B3': "Green",
    'B4': "Red",
    'B5': "NIR",
    'B6': "SWIR-1",
    'B7': "SWIR-2",
    'B8': "Panchromatic",
    'B9': "Cirrus",
    'B10': "TIRS-1",
    'B11': "TIRS-2"
}

L8_BAND_RESOLUTION = {
    'B1': "30m",
    'B2': "30m",
    'B3': "30m",
    'B4': "30m",
    'B5': "30m",
    'B6': "30m",
    'B7': "30m",
    'B8': "15m",
    'B9': "30ms",
    'B10': "30m",
    'B11': "30m"
}

# Landsat-{8,9} MTL metadata
MTL_KEY_GROUP_LANDSAT_METADATA_FILE = "LANDSAT_METADATA_FILE"
MTL_KEY_GROUP_PRODUCT_CONTENTS = "PRODUCT_CONTENTS"
MTL_KEY_GROUP_IMAGE_ATTRIBUTES = "IMAGE_ATTRIBUTES"

MTL_KEY_GROUP_LEVEL1_PROCESSING_RECORD = "LEVEL1_PROCESSING_RECORD"
MTL_KEY_GROUP_LEVEL1_MIN_MAX_RADIANCE = "LEVEL1_MIN_MAX_RADIANCE"
MTL_KEY_GROUP_LEVEL1_MIN_MAX_REFLECTANCE = "LEVEL1_MIN_MAX_REFLECTANCE"
MTL_KEY_GROUP_LEVEL1_MIN_MAX_PIXEL_VALUE = "LEVEL1_MIN_MAX_PIXEL_VALUE"
MTL_KEY_GROUP_LEVEL1_RADIOMETRIC_RESCALING = "LEVEL1_RADIOMETRIC_RESCALING"
MTL_KEY_GROUP_LEVEL1_THERMAL_CONSTANTS = "LEVEL1_THERMAL_CONSTANTS"
MTL_KEY_GROUP_LEVEL1_PROJECTION_PARAMETERS = "LEVEL1_PROJECTION_PARAMETERS"

MTL_KEY_FILE_NAME_BAND_ = "FILE_NAME_BAND_"

MTL_KEY_GROUP_PROJECTION_ATTRIBUTES = "PROJECTION_ATTRIBUTES"
MTL_KEY_MAP_PROJECTION = "MAP_PROJECTION"
MTL_KEY_DATUM = "DATUM"
MTL_KEY_ELLIPSOID = "ELLIPSOID"
MTL_KEY_UTM_ZONE = "UTM_ZONE"
MTL_KEY_GRID_CELL_SIZE_PANCHROMATIC = "GRID_CELL_SIZE_PANCHROMATIC"
MTL_KEY_GRID_CELL_SIZE_REFLECTIVE = "GRID_CELL_SIZE_REFLECTIVE"
MTL_KEY_GRID_CELL_SIZE_THERMAL = "GRID_CELL_SIZE_THERMAL"
MTL_KEY_PANCHROMATIC_LINES = "PANCHROMATIC_LINES"
MTL_KEY_PANCHROMATIC_SAMPLES = "PANCHROMATIC_SAMPLES"
MTL_KEY_REFLECTIVE_LINES = "REFLECTIVE_LINES"
MTL_KEY_REFLECTIVE_SAMPLES = "REFLECTIVE_SAMPLES"
MTL_KEY_THERMAL_LINES = "THERMAL_LINES"
MTL_KEY_THERMAL_SAMPLES = "THERMAL_SAMPLES"
MTL_KEY_CORNER_UL_LAT_PRODUCT = "CORNER_UL_LAT_PRODUCT"
MTL_KEY_CORNER_UL_LON_PRODUCT = "CORNER_UL_LON_PRODUCT"
MTL_KEY_CORNER_UR_LAT_PRODUCT = "CORNER_UR_LAT_PRODUCT"
MTL_KEY_CORNER_UR_LON_PRODUCT = "CORNER_UR_LON_PRODUCT"
MTL_KEY_CORNER_LL_LAT_PRODUCT = "CORNER_LL_LAT_PRODUCT"
MTL_KEY_CORNER_LL_LON_PRODUCT = "CORNER_LL_LON_PRODUCT"
MTL_KEY_CORNER_LR_LAT_PRODUCT = "CORNER_LR_LAT_PRODUCT"
MTL_KEY_CORNER_LR_LON_PRODUCT = "CORNER_LR_LON_PRODUCT"
MTL_KEY_CORNER_UL_PROJECTION_X_PRODUCT = "CORNER_UL_PROJECTION_X_PRODUCT"
MTL_KEY_CORNER_UL_PROJECTION_Y_PRODUCT = "CORNER_UL_PROJECTION_Y_PRODUCT"
MTL_KEY_CORNER_UR_PROJECTION_X_PRODUCT = "CORNER_UR_PROJECTION_X_PRODUCT"
MTL_KEY_CORNER_UR_PROJECTION_Y_PRODUCT = "CORNER_UR_PROJECTION_Y_PRODUCT"
MTL_KEY_CORNER_LL_PROJECTION_X_PRODUCT = "CORNER_LL_PROJECTION_X_PRODUCT"
MTL_KEY_CORNER_LL_PROJECTION_Y_PRODUCT = "CORNER_LL_PROJECTION_Y_PRODUCT"
MTL_KEY_CORNER_LR_PROJECTION_X_PRODUCT = "CORNER_LR_PROJECTION_X_PRODUCT"
MTL_KEY_CORNER_LR_PROJECTION_Y_PRODUCT = "CORNER_LR_PROJECTION_Y_PRODUCT"

# Attributes: Landsat_8, Landsat_9
META_KEY_L8_MISSION_ID: str = "mission_id"
META_KEY_L8_PROCESSING_CORRECTION_LEVEL: str = "processing_correction_level"
META_KEY_L8_WRD: str = "wrd"
META_KEY_L8_ACQUISITION_TIME: str = "acquisition_time"
META_KEY_L8_PROCESSING_TIME: str = "processing_time"
META_KEY_L8_COLLECTION_NUMBER: str = "collection_number"
META_KEY_L8_COLLECTION_CATEGORY: str = "collection_category"

META_KEY_L8_BAND_NAME: str = "band_name"
META_KEY_L8_BAND_RADIOMETRY: str = "band_radiometry"
META_KEY_L8_SPATIAL_RESOLUTION: str = "spatial_resolution"
META_KEY_L8_WIDTH: str = "width"
META_KEY_L8_HEIGHT: str = "height"
META_KEY_L8_CRS: str = "crs"

CORNER_KEY_TOP_LEFT = "TopLeft"
CORNER_KEY_BOTTOM_LEFT = "BottomLeft"
CORNER_KEY_TOP_RIGHT = "TopRight"
CORNER_KEY_BOTTOM_RIGHT = "BottomRight"

class Landsat8:
    def __init__(self):
        self.product: dict = {}
        self.product_coords: dict = {
            "lat": [],
            "lon": []
        }
        self.dataPath: str = None
        self.folderName: str = None
        self.metadata: dict = {}
        self.data: dict = {}
        self.latitude: np.ndarray = None
        self.longitude: np.ndarray = None
        self.mtl_metadata: dict = None
        self.cornerCoordinates: dict = {
            CORNER_KEY_TOP_LEFT: [0, 0],
            CORNER_KEY_BOTTOM_LEFT: [1, 0],
            CORNER_KEY_TOP_RIGHT: [0, 1],
            CORNER_KEY_BOTTOM_RIGHT: [1, 1]
        }
        self.panchromaticSize: dict = {
            META_KEY_L8_WIDTH: None,
            META_KEY_L8_HEIGHT: None
        }

    @staticmethod
    def _decode_date(in_date: str):
        year = in_date[:4]
        month = in_date[4:-2]
        day = in_date[-2:]
        return dt.datetime.strptime(f"{year}-{month}-{day}",
                                    "%Y-%m-%d").date()

    def _decode_file_name(self):
        if self.folderName is None or type(self.folderName) is not str:
            print("Error::Product has not been read yet. Run \"read\" first.")
            return False
        decoder = self.folderName.split("_")
        try:
            self.metadata[META_KEY_L8_MISSION_ID] = decoder[0]
            self.metadata[META_KEY_L8_PROCESSING_CORRECTION_LEVEL] = decoder[1]
            self.metadata[META_KEY_L8_WRD] = decoder[2]
            self.metadata[META_KEY_L8_ACQUISITION_TIME] = self._decode_date(decoder[3])
            self.metadata[META_KEY_L8_PROCESSING_TIME] = self._decode_date(decoder[4])
            self.metadata[META_KEY_L8_COLLECTION_NUMBER] = decoder[5]
            self.metadata[META_KEY_L8_COLLECTION_CATEGORY] = decoder[6]
        except:
            print("Error:Something went wrong. Look the folders name if anything wrong.")
            return False
        return True

    def _get_MTL_file(self):
        if self.dataPath is None:
            print("Error::dataPath is not specified yet. Call read for correctly specify its value!")
            return False, None

        dirFiles = os.listdir(self.dataPath)
        for __file__ in dirFiles:
            if __file__.__contains__("_MTL.txt"):
                return True, os.path.normpath(self.dataPath + "/" + __file__)

        print("Error::*_MTL.txt file does not exist in the folder.")
        return False, None

    def _read_mtl_metadata(self, mtlFilePath: str):
        """
        Read the metadata which containing in mtl file.
        :param mtlFilePath:
        :return:
        """
        # If mtlFile exits
        if os.path.exists(mtlFilePath):
            self.mtl_metadata = {}
            mtl_Lines = open(mtlFilePath, 'r').readlines()
            currKey = ""
            for __line__ in mtl_Lines:
                __line__ = __line__.replace('\n', '').replace(' ', '')
                lineData = __line__.split('=')
                if lineData[0] == 'GROUP':
                    if lineData[1] == MTL_KEY_GROUP_LANDSAT_METADATA_FILE:
                        self.mtl_metadata = {}
                    else:
                        currKey = lineData[1]
                        self.mtl_metadata[currKey] = {}
                elif lineData[0] == 'END_GROUP' or lineData[0] == 'END':
                    pass
                else:
                    try:
                        self.mtl_metadata[currKey][lineData[0]] = float(lineData[1])
                    except:
                        self.mtl_metadata[currKey][lineData[0]] = lineData[1].replace('"', '')
        else:
            print("Info::Couldn't find metadata mtl file.")

    def _does_mtl_exists(self):
        return self.mtl_metadata is not None or type(self.mtl_metadata) is dict

    def _create_metadata_from_mtl(self):
        def add_mtl_group_to_metadata(group_key):
            if group_key in self.mtl_metadata.keys():
                for __key__ in self.mtl_metadata[group_key]:
                    self.metadata[__key__] = self.mtl_metadata[group_key][__key__]

        if not self._does_mtl_exists():
            print("Error::MTL metadata does not exist.")
            return False
        else:
            add_mtl_group_to_metadata(MTL_KEY_GROUP_IMAGE_ATTRIBUTES)
            add_mtl_group_to_metadata(MTL_KEY_GROUP_PROJECTION_ATTRIBUTES)
            add_mtl_group_to_metadata(MTL_KEY_GROUP_LEVEL1_RADIOMETRIC_RESCALING)
            add_mtl_group_to_metadata(MTL_KEY_GROUP_LEVEL1_THERMAL_CONSTANTS)

        self.metadata[META_KEY_L8_CRS] = self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_DATUM]

    def _create_metadata(self):
        self._decode_file_name()
        self.metadata[META_KEY_L8_BAND_NAME] = L8_MAIN_BANDS
        self.metadata[META_KEY_L8_BAND_RADIOMETRY] = []
        for __key__ in L8_BAND_SPECTRAL.keys():
            self.metadata[META_KEY_L8_BAND_RADIOMETRY].append(L8_BAND_SPECTRAL[__key__])

        self.metadata[META_KEY_L8_SPATIAL_RESOLUTION] = []
        for __key__ in L8_BAND_RESOLUTION.keys():
            self.metadata[META_KEY_L8_SPATIAL_RESOLUTION].append(L8_BAND_RESOLUTION[__key__])


        self.metadata[META_KEY_L8_WIDTH] = 0
        self.metadata[META_KEY_L8_HEIGHT] = 0
        self.metadata[META_KEY_L8_CRS] = "WGS84"
        self._create_metadata_from_mtl()

    def _calculate_wgs84_coordinates_from_mtl(self):
        # Get panchromatic size
        if MTL_KEY_PANCHROMATIC_LINES in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.panchromaticSize[META_KEY_L8_WIDTH] = self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_PANCHROMATIC_LINES]
        if MTL_KEY_PANCHROMATIC_SAMPLES in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.panchromaticSize[META_KEY_L8_HEIGHT] = self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_PANCHROMATIC_SAMPLES]
        # TopLeft Corner
        if MTL_KEY_CORNER_UL_LAT_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_TOP_LEFT][0] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_UL_LAT_PRODUCT]
        if MTL_KEY_CORNER_UL_LON_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_TOP_LEFT][1] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_UL_LON_PRODUCT]
        # BottomLeft Corner
        if MTL_KEY_CORNER_LL_LAT_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_BOTTOM_LEFT][0] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_LL_LAT_PRODUCT]
        if MTL_KEY_CORNER_LL_LON_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_BOTTOM_LEFT][1] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_LL_LON_PRODUCT]
        # TopRight Corner
        if MTL_KEY_CORNER_UR_LAT_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_TOP_RIGHT][0] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_UR_LAT_PRODUCT]
        if MTL_KEY_CORNER_UR_LON_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_TOP_RIGHT][1] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_UR_LON_PRODUCT]
        # BottomRight Corner
        if MTL_KEY_CORNER_LR_LAT_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_BOTTOM_RIGHT][0] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_LR_LAT_PRODUCT]
        if MTL_KEY_CORNER_LR_LON_PRODUCT in self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES]:
            self.cornerCoordinates[CORNER_KEY_BOTTOM_RIGHT][1] = \
            self.mtl_metadata[MTL_KEY_GROUP_PROJECTION_ATTRIBUTES][MTL_KEY_CORNER_LR_LON_PRODUCT]


        latArr = np.array([
            self.cornerCoordinates[CORNER_KEY_TOP_LEFT][0],
            self.cornerCoordinates[CORNER_KEY_TOP_RIGHT][0],
            self.cornerCoordinates[CORNER_KEY_BOTTOM_LEFT][0],
            self.cornerCoordinates[CORNER_KEY_BOTTOM_RIGHT][0],
            ])
        lonArr = np.array([
            self.cornerCoordinates[CORNER_KEY_TOP_LEFT][1],
            self.cornerCoordinates[CORNER_KEY_TOP_RIGHT][1],
            self.cornerCoordinates[CORNER_KEY_BOTTOM_LEFT][1],
            self.cornerCoordinates[CORNER_KEY_BOTTOM_RIGHT][1],
            ])

        minLat = latArr.min()
        maxLat = latArr.max()
        minLon = lonArr.min()
        maxLon = lonArr.max()

        stepLat = (minLat - maxLat) / self.panchromaticSize[META_KEY_L8_HEIGHT]
        stepLon = (maxLon - minLon) / self.panchromaticSize[META_KEY_L8_WIDTH]

        # Lat: increase bottom->up (height dimension)
        self.product_coords["lat"] = np.arange(maxLat, minLat, stepLat)[:int(self.panchromaticSize[META_KEY_L8_HEIGHT])]

        # Lon: increase left->right (width dimension)
        self.product_coords["lon"] = np.arange(minLon, maxLon, stepLon)[:int(self.panchromaticSize[META_KEY_L8_WIDTH])]

    def _find_pixel_position_from_wgs84_coordinates(self, coordLat, coordLon):
        latDist = np.absolute(self.product_coords["lat"] - coordLat)
        px_y = np.where(latDist == latDist.min())
        px_y = px_y[0][0]

        lonDist = np.absolute(self.product_coords["lon"] - coordLon)
        px_x = np.where(lonDist == lonDist.min())
        px_x = px_x[0][0]

        return px_x, px_y

    @staticmethod
    def _calc_new_pixel_index(px_min: int, px_max: int, mul_res: int,
                              dim_size: int):
        if px_min < 0:
            px_min = 0
        if px_max >= dim_size:
            px_max = dim_size - 1

        dst = abs(px_max - px_min)
        mul = int(round(dst / mul_res))
        if mul == 0:
            mul = 1

        new_px_min = min(px_min, px_max)
        new_px_max = new_px_min + mul * mul_res

        if new_px_max > dim_size:
            new_px_max = dim_size
            if new_px_max - mul * mul_res > 0:
                new_px_min = new_px_max - mul * mul_res
            else:
                new_px_min = new_px_max - (mul - 1) * mul_res

        return new_px_min, new_px_max

    def  _read_data(self, minLat=None, maxLat=None,
            minLon=None, maxLon=None, imgResolution=None):

        if self._does_mtl_exists() and MTL_KEY_GROUP_PRODUCT_CONTENTS in self.mtl_metadata.keys():

            self._calculate_wgs84_coordinates_from_mtl()

            # Clip Coordinates
            MIN_clipped = [self.product_coords["lat"][-1], self.product_coords["lon"][0]]
            MAX_clipped = [self.product_coords["lat"][0], self.product_coords["lon"][-1]]

            if (MIN_clipped[0] < minLat < MAX_clipped[0] and
                    MIN_clipped[0] < maxLat < MAX_clipped[0]):
                if minLat is not None:
                    if maxLat is not None:
                        MIN_clipped[0] = min(minLat, maxLat)
                        MAX_clipped[0] = max(minLat, maxLat)
                    else:
                        MIN_clipped[0] = minLat
                elif maxLat is not None:
                    MAX_clipped[0] = maxLat

            if (MIN_clipped[1] < minLon < MAX_clipped[1] and
                    MIN_clipped[1] < maxLon < MAX_clipped[1]):
                if minLon is not None:
                    if maxLon is not None:
                        MIN_clipped[1] = min(minLon, maxLon)
                        MAX_clipped[1] = max(minLon, maxLon)
                    else:
                        MIN_clipped[1] = minLon
                elif maxLon is not None:
                    MAX_clipped[1] = maxLon

            min_px_x, min_px_y = self._find_pixel_position_from_wgs84_coordinates(MAX_clipped[0], MIN_clipped[1])
            max_px_x, max_px_y = self._find_pixel_position_from_wgs84_coordinates(MIN_clipped[0], MAX_clipped[1])

            min_px_x, max_px_x = self._calc_new_pixel_index(int(min_px_x),
                                                            int(max_px_x),
                                                            imgResolution,
                                                            self.panchromaticSize[META_KEY_L8_WIDTH])
            min_px_y, max_px_y = self._calc_new_pixel_index(int(min_px_y),
                                                            int(max_px_y),
                                                            imgResolution,
                                                            self.panchromaticSize[META_KEY_L8_HEIGHT])

            self.latitude = self.product_coords["lat"][int(min_px_y):int(max_px_y):1]
            self.longitude = self.product_coords["lon"][int(min_px_x):int(max_px_x):1]

            self.metadata[META_KEY_L8_WIDTH] = max_px_x - min_px_x
            self.metadata[META_KEY_L8_HEIGHT] = max_px_y - min_px_y

            l_file_names = []
            pan_width = self.panchromaticSize[META_KEY_L8_WIDTH]
            pan_height = self.panchromaticSize[META_KEY_L8_HEIGHT]
            for __key__ in self.mtl_metadata[MTL_KEY_GROUP_PRODUCT_CONTENTS].keys():
                if type(self.mtl_metadata[MTL_KEY_GROUP_PRODUCT_CONTENTS][__key__]) is str and __key__.__contains__(MTL_KEY_FILE_NAME_BAND_):
                    l_file_names.append(self.mtl_metadata[MTL_KEY_GROUP_PRODUCT_CONTENTS][__key__])
            for __fileName__ in l_file_names:
                filePath = os.path.normpath(self.dataPath + "/" + __fileName__)
                bandName = __fileName__.split("_")[-1].split(".")[0]
                self.product[bandName] = rs.open(filePath, 'r')
                img = self.product[bandName].read()
                bandMaxValue = img.max()
                if bandMaxValue == 0:
                    bandMaxValue = 1
                tmp_img = np.array(np.array(img, dtype="float32") / bandMaxValue)[0]
                if pan_width != 0 and pan_height != 0:
                    ratio_w = int((pan_width / tmp_img.shape[0]).__round__())
                    ratio_h = int((pan_height / tmp_img.shape[1]).__round__())
                    tmp_img = np.kron(tmp_img, np.ones((ratio_w, ratio_h)))

                    self.data[bandName] = np.zeros((max_px_y-min_px_y, max_px_x-min_px_x))
                    self.data[bandName][0:max_px_y-min_px_y,
                                        0:max_px_x-min_px_x] = tmp_img[min_px_y:max_px_y,
                                                                       min_px_x:max_px_x]
                else:
                    self.data[bandName] = tmp_img
        else:
            pass
        return True

    def read(self, dirPath: str,
             minLat: float = None, maxLat: float = None,
             minLon: float = None, maxLon: float = None,
             imgResolution: int = None):
        # Error checking
        if not os.path.isdir(dirPath) and not dirPath.__contains__(".tar"):
            print("Error::Unknown data format!")
            return False
            # If passing the error checking
        else:
            self.dataPath = dirPath  # Store the path to a tmp variable
            # Check if the path is a TAR format
            if self.dataPath.__contains__(".tar"):
                self.dataPath = dirPath.split(".tar")[0]  # get the path without the TAR extension
                tar = tarfile.open(dirPath)  # open the TAR file
                tar.extractall(self.dataPath)  # unTAR the data

            self.folderName = os.path.basename(self.dataPath)

            mtl_exists, mtl_path = self._get_MTL_file()
            if mtl_exists:
                self._read_mtl_metadata(mtl_path)
            self._create_metadata()
            self._read_data(minLat=minLat, maxLat=maxLat,
                            minLon=minLon, maxLon=maxLon,
                            imgResolution=imgResolution)


    def showImage_Gray(self, band_id: int):
        if self.data.keys().__len__() == 0:
            return False

        if band_id == 0:
            band_id = 1

        bandName = self.metadata[META_KEY_L8_BAND_NAME][band_id-1]

        print(f"GRAY= {bandName}")

        img = self.data[bandName] # Get image
        # Change the radiometry to uint8
        maxVal = img.max()
        if maxVal == 0:
            maxVal = 1
        img = np.array(img / maxVal * 256, dtype=np.uint8)
        img = Image.fromarray(img)  # Create a PIL image from the array
        img.show()  # Show the Image

        return True

    def showImage_RGB(self, red_id: int, green_id: int, blue_id: int):
        if self.data.keys().__len__() < 3:
            return False

        if red_id <= 0:
            red_id = 1
        if green_id <= 0:
            green_id = 1
        if blue_id <= 0:
            blue_id = 1

        redName = self.metadata[META_KEY_L8_BAND_NAME][red_id-1]
        greenName = self.metadata[META_KEY_L8_BAND_NAME][green_id-1]
        blueName = self.metadata[META_KEY_L8_BAND_NAME][blue_id-1]

        print(f"RED= {redName}\nGREEN= {greenName}\nBLUE= {blueName}")

        # Get image
        red_img = self.data[redName]
        green_img = self.data[greenName]
        blue_img = self.data[blueName]

        redMax = red_img.max()
        if redMax == 0:
            redMax = 1

        greenMax = green_img.max()
        if greenMax == 0:
            greenMax = 1

        blueMax = blue_img.max()
        if blueMax == 0:
            blueMax = 1

        # Change the radiometry to uint8
        red_img = np.array(red_img / redMax * 256, dtype=np.uint8).T
        green_img = np.array(green_img / greenMax * 256, dtype=np.uint8).T
        blue_img = np.array(blue_img / blueMax * 256, dtype=np.uint8).T

        img = Image.fromarray(np.array([red_img, green_img, blue_img]).T)  # Create a PIL image from the array
        img.show()  # Show the Image

        return True

    def getMetadata(self):
        return self.metadata

    def getData(self):
        return self.data

    def getLatitudes(self):
        return self.latitude

    def getLongitudes(self):
        return self.longitude

if __name__ == "__main__":
    PATH = "../../Data/Landsat/LC08_L1TP_180035_20230726_20230726_02_RT.tar"
    FILTER_MIN_LAT = 35.82
    FILTER_MIN_LON = 27.52
    FILTER_MAX_LAT = 36.48
    FILTER_MAX_LON = 28.34
    FILTER_RESOLUTION_MULTIPLIED = 512

    l8 = Landsat8()
    l8.read(dirPath=PATH,
            minLat=FILTER_MIN_LAT,
            maxLat=FILTER_MAX_LAT,
            minLon=FILTER_MIN_LON,
            maxLon=FILTER_MAX_LON,
            imgResolution=FILTER_RESOLUTION_MULTIPLIED
            )
