import os
import tarfile
import shutil

import datetime as dt
import numpy as np
import xarray as xr

import satellitedatacube.src.sat.Sentinel_2 as sen2
import satellitedatacube.src.sat.Landsat_8 as lan8
import satellitedatacube.src.lib.fileManipulation as file_manip

from zipfile import ZipFile

SENTINEL_1: str = "Sentinel-1"
SENTINEL_2: str = "Sentinel-2"
SENTINEL_3: str = "Sentinel-3"
LANDSAT_8: str = "Landsat-8"
LANDSAT_9: str = "Landsat-9"

_DECODER_SATELLITE_INFO = {
    'S1A': SENTINEL_1,
    'S1B': SENTINEL_1,
    'S2A': SENTINEL_2,
    'S2B': SENTINEL_2,
    'S3A': SENTINEL_3,
    'S3B': SENTINEL_3,
    'S3C': SENTINEL_3,
    'LC08': LANDSAT_8,
    'LC09': LANDSAT_9,
}

KEY_DIMS: str = "dims"
KEY_COORDS: str = "coords"
KEY_DATA: str = "data"
KEY_DATA_VARS: str = "data_vars"
KEY_ATTRS: str = "attrs"

KEY_Y: str = "y"
KEY_X: str = "x"
KEY_TIME: str = "time"

KEY_ATTRS_MISSION_FAMILY: str = "mission_family"

DATACUBE_DIMENSIONS = (KEY_TIME, KEY_Y, KEY_X)


class Datacubes:
    def __init__(self):
        self._datacube = {}

    @staticmethod
    def _get_empty_xarray_dataset():
        ds_xarray = {
            KEY_COORDS: {

            },
            KEY_ATTRS: {

            }.copy(),
            KEY_DIMS: {
                KEY_TIME: 0,
                KEY_Y: 0,
                KEY_X: 0
            },
            KEY_DATA_VARS: {

            }
        }
        return ds_xarray.copy()

    @staticmethod
    def _get_empty_xarray_entry():
        entry_xarray = {
            KEY_DIMS: "dim",
            KEY_DATA: []
        }
        return entry_xarray.copy()

    @staticmethod
    def _get_appropriate_date_value(in_value):
        if type(in_value) is dt.datetime or type(in_value) is dt.date:
            # if the type is datetime.datetime
            if type(in_value) is dt.datetime:
                return in_value.date()  # then get the datetime.date type
            else:  # Otherwise is datetime.date (previous checks)
                return in_value  # append it as is
        return None

    @staticmethod
    def _get_appropriate_coord_value(in_value):
        if type(in_value) is float:
            return in_value
        elif type(in_value) is int:
            return float(in_value)
        return None

    @staticmethod
    def _decodeDate(in_date):
        year = int(in_date.year)
        month = int(in_date.month)
        day = int(in_date.day)

        return "%04d-%02d-%02d" % (year, month, day)

    def _add_Sentinel_2(self, folderPath: str, satMission: str,
                        f_startDate=None, f_endDate=None,
                        f_minLat: float = None, f_maxLat: float = None,
                        f_minLon: float = None, f_maxLon: float = None,
                        f_resolution_multiple: int = None):
        """

        :param folderPath:
        :param satMission:
        :param f_startDate:
        :param f_endDate:
        :param f_minLat:
        :param f_maxLat:
        :param f_minLon:
        :param f_maxLon:
        :param f_resolution_multiple:
        :return:
        """

        # ----------------------------------------------
        # Define boolean functions
        def isCommonKey(key: str):
            return key is sen2.META_KEY_S2_BAND_NAME or \
                key is sen2.META_KEY_S2_BAND_RADIOMETRY or \
                key is sen2.META_KEY_S2_SPATIAL_RESOLUTION or \
                key is sen2.META_KEY_S2_WIDTH or \
                key is sen2.META_KEY_S2_HEIGHT or \
                key is sen2.META_KEY_S2_CRS

        def isDateKey(key: str):
            return key is sen2.META_KEY_S2_CREATE_DATE or \
                key is sen2.META_KEY_S2_SENSING_DATE_STOP

        # ----------------------------------------------
        # ----------------------------------------------
        # Check if the satellite mission is Sentinel-2
        if satMission is not SENTINEL_2:
            print(
                f"Error::_add_Sentinel_2 needs a \"{SENTINEL_2}\" mission product, "
                f"otherwise the process will be crashed.")
            return False

        # Read product
        s2 = sen2.Sentinel2()
        s2.read(dirPath=folderPath,
                minLat=f_minLat, maxLat=f_maxLat,
                minLon=f_minLon, maxLon=f_maxLon,
                imgResolution=f_resolution_multiple)

        # ----------------------------------------------
        # ----------------------------------------------
        # Check if the product is outside the given date range
        createDate = s2.metadata[sen2.META_KEY_S2_CREATE_DATE]
        if (f_startDate is not None and createDate < f_startDate) or \
                (f_endDate is not None and createDate > f_endDate):
            print("Info::Image filtered out by date.")
            return False

        # Check if this is the first time of adding data
        metadata = s2.getMetadata()
        if satMission not in self._datacube.keys():
            self._datacube[satMission] = self._get_empty_xarray_dataset()

            self._datacube[satMission][KEY_COORDS][
                KEY_X] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_X][KEY_DIMS] = "x"
            self._datacube[satMission][KEY_COORDS][KEY_X][
                KEY_DATA] = s2.getLongitudes()

            self._datacube[satMission][KEY_COORDS][
                KEY_Y] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_Y][KEY_DIMS] = "y"
            self._datacube[satMission][KEY_COORDS][KEY_Y][
                KEY_DATA] = s2.getLatitudes()

            self._datacube[satMission][KEY_COORDS][
                KEY_TIME] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_TIME][
                KEY_DIMS] = "time"
            self._datacube[satMission][KEY_COORDS][KEY_TIME][KEY_DATA] = []

            self._datacube[satMission][KEY_ATTRS][
                KEY_ATTRS_MISSION_FAMILY] = satMission
            # Create metadata
            for __key__ in metadata.keys():
                # The following if exclude common attributes between all data
                if isCommonKey(__key__):
                    self._datacube[satMission][KEY_ATTRS][__key__] = \
                    metadata[__key__]
                else:  # else add an empty array for appending values
                    self._datacube[satMission][KEY_ATTRS][
                        __key__] = [].copy()

            # Create data
            for __key__ in s2.getData().keys():
                self._datacube[satMission][KEY_DATA_VARS][
                    __key__] = self._get_empty_xarray_entry()
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DIMS] = DATACUBE_DIMENSIONS
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DATA] = [s2.getData().copy()[__key__]]

                if self._datacube[satMission][KEY_DIMS][KEY_Y] == 0 or self._datacube[satMission][KEY_DIMS][KEY_X] == 0:
                    dataSize = np.array(
                        self._datacube[satMission][KEY_DATA_VARS][__key__][
                            KEY_DATA]).shape
                    self._datacube[satMission][KEY_DIMS][KEY_Y] = dataSize[1]
                    self._datacube[satMission][KEY_DIMS][KEY_X] = dataSize[2]
            self._datacube[satMission][KEY_DIMS][KEY_TIME] += 1

        else:
            # Create data
            for __key__ in s2.getData().keys():
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DATA].append(s2.getData().copy()[__key__])
            self._datacube[satMission][KEY_DIMS][KEY_TIME] += 1

        self._datacube[satMission][KEY_COORDS][KEY_TIME][KEY_DATA].append(
            self._decodeDate(createDate))

        # This for is used for appending metadata to the arrays
        for __key__ in metadata.keys():
            # Exclude from appending already created data
            if isCommonKey(__key__):
                pass
            elif isDateKey(__key__):
                self._datacube[satMission][KEY_ATTRS][__key__].append(
                    self._decodeDate(metadata[__key__]))
            else:
                self._datacube[satMission][KEY_ATTRS][__key__].append(
                    metadata[__key__])

        return True

    def _add_Landsat_8(self, folderPath: str, satMission: str,
                       f_startDate=None, f_endDate=None,
                       f_minLat: float = None, f_maxLat: float = None,
                       f_minLon: float = None, f_maxLon: float = None,
                       f_resolution_multiple: int = None):

        def isCommonKey(key: str):
            return key is lan8.META_KEY_L8_BAND_NAME or \
                key is lan8.META_KEY_L8_BAND_RADIOMETRY or \
                key is lan8.META_KEY_L8_SPATIAL_RESOLUTION or \
                key is lan8.META_KEY_L8_WIDTH or \
                key is lan8.META_KEY_L8_HEIGHT or \
                key is lan8.META_KEY_L8_CRS

        def isDateKey(key: str):
            return key is lan8.META_KEY_L8_ACQUISITION_TIME or \
                key is lan8.META_KEY_L8_PROCESSING_TIME

        # ----------------------------------------------
        # ----------------------------------------------
        # Check if the satellite mission is Sentinel-2
        if satMission is not _LANDSAT_8 and satMission is not _LANDSAT_9:
            print(
                f"Error::_add_Landsat_8 needs a \"{_LANDSAT_8}\" or \"{_LANDSAT_9}\" missions products, "
                f"otherwise the process will be failed.")
            return False

        # Read product
        l8 = lan8.Landsat8()
        l8.read(dirPath=folderPath,
                minLat=f_minLat, maxLat=f_maxLat,
                minLon=f_minLon, maxLon=f_maxLon,
                imgResolution=f_resolution_multiple)

        # ----------------------------------------------
        # ----------------------------------------------
        # Check if the product is outside the given date range
        createDate = l8.metadata[lan8.META_KEY_L8_ACQUISITION_TIME]
        if (f_startDate is not None and createDate < f_startDate) or \
                (f_endDate is not None and createDate > f_endDate):
            print("Info::Image filtered out by date.")
            return False

        # Check if this is the first time of adding data
        metadata = l8.getMetadata()
        if satMission not in self._datacube.keys():
            self._datacube[satMission] = self._get_empty_xarray_dataset()

            self._datacube[satMission][KEY_COORDS][
                KEY_X] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_X][KEY_DIMS] = "x"
            self._datacube[satMission][KEY_COORDS][KEY_X][
                KEY_DATA] = l8.getLongitudes()

            self._datacube[satMission][KEY_COORDS][
                KEY_Y] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_Y][KEY_DIMS] = "y"
            self._datacube[satMission][KEY_COORDS][KEY_Y][
                KEY_DATA] = l8.getLatitudes()

            self._datacube[satMission][KEY_COORDS][
                KEY_TIME] = self._get_empty_xarray_entry()
            self._datacube[satMission][KEY_COORDS][KEY_TIME][
                KEY_DIMS] = "time"
            self._datacube[satMission][KEY_COORDS][KEY_TIME][KEY_DATA] = []

            self._datacube[satMission][KEY_ATTRS][
                KEY_ATTRS_MISSION_FAMILY] = satMission
            # Create metadata
            for __key__ in metadata.keys():
                # The following if exclude common attributes between all data
                if isCommonKey(__key__):
                    self._datacube[satMission][KEY_ATTRS][__key__] = \
                    metadata[__key__]
                else:  # else add an empty array for appending values
                    self._datacube[satMission][KEY_ATTRS][
                        __key__] = [].copy()

            # Create data
            for __key__ in l8.getData().keys():
                self._datacube[satMission][KEY_DATA_VARS][
                    __key__] = self._get_empty_xarray_entry()
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DIMS] = DATACUBE_DIMENSIONS
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DATA] = [l8.getData().copy()[__key__]]

                if self._datacube[satMission][KEY_DIMS][KEY_Y] == 0 or self._datacube[satMission][KEY_DIMS][KEY_X] == 0:
                    dataSize = np.array(self._datacube[satMission][KEY_DATA_VARS][__key__][KEY_DATA]).shape
                    self._datacube[satMission][KEY_DIMS][KEY_Y] = dataSize[1]
                    self._datacube[satMission][KEY_DIMS][KEY_X] = dataSize[2]
            self._datacube[satMission][KEY_DIMS][KEY_TIME] += 1

        else:
            # Create data
            for __key__ in l8.getData().keys():
                self._datacube[satMission][KEY_DATA_VARS][__key__][
                    KEY_DATA].append(l8.getData().copy()[__key__])
            self._datacube[satMission][KEY_DIMS][KEY_TIME] += 1

        self._datacube[satMission][KEY_COORDS][KEY_TIME][KEY_DATA].append(
            self._decodeDate(createDate))

        # This for is used for appending metadata to the arrays
        for __key__ in metadata.keys():
            # Exclude from appending already created data
            if isCommonKey(__key__):
                pass
            elif isDateKey(__key__):
                self._datacube[satMission][KEY_ATTRS][__key__].append(
                    self._decodeDate(metadata[__key__]))
            else:
                self._datacube[satMission][KEY_ATTRS][__key__].append(
                    metadata[__key__])

        return True

    def _add_to_cube(self, folderPath: str, f_startDate=None,
                     f_endDate=None,
                     f_minLat: float = None, f_maxLat: float = None,
                     f_minLon: float = None, f_maxLon: float = None,
                     f_resolution_multiple: int = None):

        # Set the dateRange filter. If acceptable Start and End dates, then add them as dt.date type, otherwise append None
        dateRange = [self._get_appropriate_date_value(f_startDate),
                     self._get_appropriate_date_value(f_endDate)]

        # Set the coordRange filter. If acceptable lat and lon coords, then add them as a float type, otherwise append None
        coordRange = [self._get_appropriate_coord_value(f_minLat),
                      self._get_appropriate_coord_value(f_minLon),
                      self._get_appropriate_coord_value(f_maxLat),
                      self._get_appropriate_coord_value(f_maxLon)]

        folderBasename = os.path.basename(folderPath)
        satKey = folderBasename.split("_")[0]
        if satKey not in _DECODER_SATELLITE_INFO.keys():
            print(
                "Error::Unknown satellite identifier. Please check the folder's name if it's correct.")
            return False

        success = False

        satMission = _DECODER_SATELLITE_INFO[satKey]
        if satMission is SENTINEL_1:
            print("Info::This mission is not supported yet!")
        elif satMission is SENTINEL_2:
            print("Info::Try reading Sentinel-2 Mission")
            success = self._add_Sentinel_2(
                folderPath=folderPath, satMission=satMission,
                f_startDate=dateRange[0], f_endDate=dateRange[1],
                f_minLat=coordRange[0], f_maxLat=coordRange[2],
                f_minLon=coordRange[1], f_maxLon=coordRange[3],
                f_resolution_multiple=f_resolution_multiple
            )

        elif satMission is SENTINEL_3:
            print("Info::This mission is not supported yet!")
        elif satMission is LANDSAT_8 or satMission is LANDSAT_9:
            print(
                f"Info::Try reading {LANDSAT_8} or {LANDSAT_9}  Mission")
            success = self._add_Landsat_8(
                folderPath=folderPath, satMission=satMission,
                f_startDate=dateRange[0], f_endDate=dateRange[1],
                f_minLat=coordRange[0], f_maxLat=coordRange[2],
                f_minLon=coordRange[1], f_maxLon=coordRange[3],
                f_resolution_multiple=f_resolution_multiple
            )
        else:
            print("Error::Unknown satellite mission.")

        return success

    def create_datacube_from_folder(self, folderPath: str,
                                    f_startDate=None, f_endDate=None,
                                    f_minLat: float = None,
                                    f_maxLat: float = None,
                                    f_minLon: float = None,
                                    f_maxLon: float = None,
                                    f_resolution_multiple: int = None):
        # Get all the content of the folderPath
        # Get all files/folders in folderPath
        dataFileNames = os.listdir(folderPath)

        for __fileName__ in dataFileNames:  # Parse all files/folders in dataFileNames list
            # Create the full structured dataPath
            success = False
            dataPath = os.path.normpath(folderPath) + os.path.normpath(
                "/" + __fileName__)
            # if the path is a directory (i.e., sentinel product)
            if os.path.isdir(dataPath):
                # call _createDatacube for trying to create the dataCube
                success = self._add_to_cube(folderPath=dataPath,
                                            f_startDate=f_startDate,
                                            f_endDate=f_endDate,
                                            f_minLat=f_minLat,
                                            f_maxLat=f_maxLat,
                                            f_minLon=f_minLon,
                                            f_maxLon=f_maxLon,
                                            f_resolution_multiple=f_resolution_multiple
                                            )

            # if the path is zipped (it can be a sentinel product)
            elif dataPath.__contains__('.zip'):
                with ZipFile(dataPath) as zObject:
                    dirPath = dataPath.split(".zip")[0]
                    zObject.extractall(dirPath + '/..')
                    success = self._add_to_cube(folderPath=dirPath,
                                                f_startDate=f_startDate,
                                                f_endDate=f_endDate,
                                                f_minLat=f_minLat,
                                                f_maxLat=f_maxLat,
                                                f_minLon=f_minLon,
                                                f_maxLon=f_maxLon,
                                                f_resolution_multiple=f_resolution_multiple
                                                )
                    shutil.rmtree(dirPath)

            # if the path is zipped (it can be a landsat zipped product)
            elif dataPath.__contains__('.tar'):
                tar = tarfile.open(dataPath)
                dirPath = dataPath.split(".tar")[0]
                tar.extractall(dirPath + '/..')
                success = self._add_to_cube(folderPath=dirPath,
                                            f_startDate=f_startDate,
                                            f_endDate=f_endDate,
                                            f_minLat=f_minLat,
                                            f_maxLat=f_maxLat,
                                            f_minLon=f_minLon,
                                            f_maxLon=f_maxLon,
                                            f_resolution_multiple=f_resolution_multiple
                                            )
                shutil.rmtree(dirPath)

            else:
                success = False

            if success:
                print(
                    f"File: {__fileName__} ADDED SUCCESSFULLY to  Datacube!")
            else:
                print(
                    f"File: {__fileName__} CANNOT BE ADDED to  Datacube!")

    def getDatacube(self, satelliteMission, as_dict=False):
        """
        Return the Harmonia Datacube either as a dictionary (if as_dict=True) or
        as xarray (if as_dict=False). The default behaviour is to return the
        Datacube as xarray.

        :param satelliteMission:
        :param as_dict:
        :return:
        """
        if satelliteMission in self._datacube.keys():
            if as_dict:
                return True, self._datacube[satelliteMission].copy()
            return True, xr.Dataset.from_dict(
                self._datacube[satelliteMission]).copy()
        print(
            f"Error::Unable to find datacube for satellite mission equal to \"{satelliteMission}\"")
        return False, None

    def setDatacube(self, newCube: str, satelliteMission: str):
        self._datacube[satelliteMission] = newCube

    def addDataLayer(self, values: [], satelliteMission: str, label_name: str = 'label'):
        self._datacube[satelliteMission][KEY_DATA_VARS][label_name] = {
            KEY_ATTRS: {},
            KEY_DATA: values,
            KEY_DIMS: DATACUBE_DIMENSIONS
        }

    def exportNetCDF(self, path: str, satelliteMission: str):
        file_manip.checkAndCreateFolders(os.path.dirname(path))
        success, ds = self.getDatacube(satelliteMission)
        if success:
            ds.to_netcdf(path)
            print(
                f"Info::NetCDF file was exported to path {path} successfully!")
            return True
        return False

    def importNetCDF(self, path: str, satelliteMission: str):
        ds = xr.open_dataset(path)
        self._datacube[satelliteMission] = ds.to_dict()


if __name__ == "__main__":

    cities = {"Piraeus": {},
              "Ixelles": {},
              "Sofia": {},
              "Milano": {}
              }

    # PATH_DATA_FOLDER = '../Data/Sentinel-2/Piraeus/'
    PATH_DATA_FOLDER = '../Data/Landsat'
    # PATH_NC = '../Export/NetCDF/Piraeus.nc'
    PATH_NC = '../Export/NetCDF/Rhodes.nc'
    # FILTER_START_DATE = dt.date(2023, 7, 1)
    # FILTER_END_DATE = dt.date(2023, 8, 1)

    FILTER_START_DATE = None
    FILTER_END_DATE = None

    # FILTER_MIN_LAT = 37.921371
    # FILTER_MIN_LON = 23.597415
    # FILTER_MAX_LAT = 37.963811
    # FILTER_MAX_LON = 23.672775

    FILTER_MIN_LAT = 35.82
    FILTER_MIN_LON = 27.52
    FILTER_MAX_LAT = 36.48
    FILTER_MAX_LON = 28.34
    FILTER_RES_MUL = 512

    dc = Datacubes()
    dc.create_datacube_from_folder(folderPath=PATH_DATA_FOLDER,
                                   f_startDate=FILTER_START_DATE,
                                   f_endDate=FILTER_END_DATE,
                                   f_minLat=FILTER_MIN_LAT,
                                   f_maxLat=FILTER_MAX_LAT,
                                   f_minLon=FILTER_MIN_LON,
                                   f_maxLon=FILTER_MAX_LON,
                                   f_resolution_multiple=FILTER_RES_MUL)
    dc.exportNetCDF(path=PATH_NC, satelliteMission=LANDSAT_8)
