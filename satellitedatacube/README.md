# SatelliteDatacube

## Getting Started
The development of this library code is aiming to create a quick tool for merging multiple
satellite images to a single datacube, which can be exported as NetCDF format for further
editing. This can be achieved by storing multiple RAW Copernicus and Landsat data to a
single directory and passing the directory along with other filtering commands to the 
**create_datacube_from_folder()** function.

**Note** that the current version supports only Sentinel-2 and Landsat-{8, 9} imagery.
The development of this package is still in prototyping version.

## Dependencies
For this tool to work successfully, the following Python packages are needed to be installed
in the venv (versions in parentheses are the ones the application has been tested with):
- numpy (==1.24.3)
- datetime (==latest)
- zipfile (==latest)
- tarfile (==latest)
- pillow (==9.5.0)
- snappy (ESA version 8.0.9)
- rasterio (==1.3.8)
- xarray (==2023.6.0)

## Installation
Most of the packages can be simply installed by using the **pip** command.
Use the guide in the link (https://gitlab.com/JohnCrabs/installation_esa_snappy)
for the installation of **snappy**.

## Documentation
For a successful execution of this tool, you can run the following code
(remember to change the option capitalised flags to your value of choice):

```
import src.Datacubes as cube

PATH_TO_DATA_FOLDER = "set your path here"
PATH_EXPORT_NET_CDF = "set your path here

# Acceptable values dt.date() or None
FILTER_START_DATE = None
FILTER_END_DATE = None

# Acceptable values float coordinate values or None
FILTER_MIN_LAT = None
FILTER_MIN_LON = None
FILTER_MAX_LAT = None
FILTER_MAX_LON = None

# Acceptable values an integer rectangle resolution or None
FILTER_RESOLUTION_MULTIPLIED = None

datacube = cube.Datacubes()
datacube.create_datacube_from_folder(PATH_TO_DATA_FOLDER,
                                     f_startDate=FILTER_START_DATE,
                                     f_endDate=FILTER_END_DATE,
                                     f_minLat=FILTER_MIN_LAT,
                                     f_minLon=FILTER_MIN_LON,
                                     f_maxLat=FILTER_MAX_LAT,
                                     f_maxLon=FILTER_MAX_LON,
                                     f_resolution_multiple=FILTER_RESOLUTION_MULTIPLIED,
                                    )
datacube.exportNetCDF(PATH_EXPORT_NET_CDF, cube._SENTINEL_2)
```

## License
This project is licensed under the GNU General Public License v3.0 or later.

