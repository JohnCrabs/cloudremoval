import os
import keras
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, \
    Activation, MaxPool2D, Conv2DTranspose, \
    Concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, \
    ReduceLROnPlateau, CSVLogger
from tensorflow.keras import backend as K


class UnetCR:
    def __init__(self, input_shape: tuple = (512, 512, 1), unet_path: str = None):
        self.model = None

        if unet_path is None or not os.path.exists(unet_path):
            self.model = self.build_unet(input_shape=input_shape)
            self.model.summary()
        else:
            self.model = tf.keras.models.load_model(unet_path)
            self.model.summary()

    # EVALUATION METRICS
    @staticmethod
    def metric_recall(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall, recall.numpy()

    @staticmethod
    def metric_precision(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision, precision.numpy()

    @staticmethod
    def metric_f1(y_true, y_pred):
        precision, _ = precision_m(y_true, y_pred)
        recall, _ = recall_m(y_true, y_pred)
        f1_score = 2 * (precision * recall) / (
                precision + recall + K.epsilon())
        return f1_score.numpy()

    # BUILDING UNET
    # convolution block
    @staticmethod
    def conv_block(inputs, num_filters):
        x = Conv2D(num_filters, 3, padding="same")(inputs)
        x = BatchNormalization()(x)
        x = Activation("relu")(x)

        x = Conv2D(num_filters, 3, padding="same")(x)
        x = BatchNormalization()(x)
        x = Activation("relu")(x)

        return x

    # encoder block
    def encoder_block(self, inputs, num_filters):
        x = self.conv_block(inputs, num_filters)
        p = MaxPool2D((2, 2))(x)
        return x, p

    # decoder block
    def decoder_block(self, inputs, skip, num_filters):
        x = Conv2DTranspose(num_filters, (2, 2), strides=2,
                            padding="same")(inputs)
        x = Concatenate()([x, skip])
        x = self.conv_block(x, num_filters)
        return x

    # unet4sentinel2
    def build_unet(self, input_shape):
        inputs = Input(input_shape)
        # Encoder
        s1, p1 = self.encoder_block(inputs, 64)
        s2, p2 = self.encoder_block(p1, 128)
        s3, p3 = self.encoder_block(p2, 256)
        s4, p4 = self.encoder_block(p3, 512)

        # Bridge
        b1 = self.conv_block(p4, 1024)

        # Decoder
        d1 = self.decoder_block(b1, s4, 512)
        d2 = self.decoder_block(d1, s3, 256)
        d3 = self.decoder_block(d2, s2, 128)
        d4 = self.decoder_block(d3, s1, 64)

        outputs = Conv2D(1, 1, padding="same", activation="sigmoid")(d4)

        model = Model(inputs, outputs, name="UNET")
        return model

    # UTILITIES
    def train(self, X, y):
        pass

    def eval(self, X, y_real):
        pass

    def predict(self, X):
        if self.model is not None:
            y_pred = self.model.predict(X)
            return y_pred
        return np.zeros(np.array(X).shape)
