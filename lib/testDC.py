import satellitedatacube.src.Datacubes as dc
from PIL import Image
import numpy as np

PATH_NC = '../data/Sentinel-2/Piraeus/Sentinel-2_datacube.nc'


def showImageRGB(red, green, blue):
    r = np.array(red)
    g = np.array(green)
    b = np.array(blue)

    r = (r / r.max()) * 255
    g = (g / g.max()) * 255
    b = (b / b.max()) * 255

    img = Image.fromarray(np.array([r.T, g.T, b.T], dtype=np.uint8).T,
                          'RGB')
    img.show("Image")


def showImageGRAY(gray):
    g = np.array(gray)
    g = g / g.max() * 255
    img = Image.fromarray(np.array(g, dtype=np.uint8))
    img.show("Image")


if __name__ == "__main__":
    cube = dc.Datacubes()
    cube.importNetCDF(PATH_NC, dc.SENTINEL_2)

    _, df = cube.getDatacube(dc.SENTINEL_2, True)

    showImageGRAY(df['data_vars']['cloud_mask']['data'][0])
    showImageRGB(df['data_vars']['B4']['data'][0],
                 df['data_vars']['B3']['data'][0],
                 df['data_vars']['B2']['data'][0])
